# Translation of plasma-dialer.po to Catalan
# Copyright (C) 2020-2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-dialer\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-14 00:48+0000\n"
"PO-Revision-Date: 2023-03-02 11:07+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 20.12.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Josep M. Ferrer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "txemaq@gmail.com"

#: src/main.cpp:136 src/qml/main.qml:35
#, kde-format
msgid "Phone"
msgstr "Telèfon"

#: src/main.cpp:138
#, kde-format
msgid "Plasma phone dialer"
msgstr "Marcador de telèfon del Plasma"

#: src/main.cpp:140
#, kde-format
msgid "© 2015-2022 KDE Community"
msgstr "© 2015-2022 KDE Community"

#: src/main.cpp:143
#, kde-format
msgid "Alexey Andreyev"
msgstr "Alexey Andreyev"

#: src/qml/call/AsymmetricAnswerSwipe.qml:86
#, kde-format
msgid "Swipe to accept"
msgstr "Llisca per acceptar"

#: src/qml/call/CallPage.qml:35 src/qml/call/InCallInlineMessage.qml:14
#, kde-format
msgid "Active call list"
msgstr "Llista de trucades actives"

#: src/qml/call/CallPage.qml:108
#, kde-format
msgid "Incoming..."
msgstr "Trucada entrant..."

#: src/qml/call/CallPage.qml:111
#, kde-format
msgid "Calling..."
msgstr "S'està trucant..."

#: src/qml/call/CallPage.qml:134
#, kde-format
msgid "Keypad"
msgstr "Teclat numèric"

#: src/qml/call/CallPage.qml:154
#, kde-format
msgid "Speaker"
msgstr "Altaveu"

#: src/qml/call/CallPage.qml:168
#, kde-format
msgid "Mute"
msgstr "Silencia"

#: src/qml/call/InCallInlineMessage.qml:18
#, kde-format
msgid "View"
msgstr "Visualitza"

#: src/qml/components/BottomToolbar.qml:81 src/qml/components/Sidebar.qml:38
#, kde-format
msgid "History"
msgstr "Historial"

#: src/qml/components/BottomToolbar.qml:93 src/qml/components/Sidebar.qml:56
#: src/qml/ContactsPage.qml:19
#, kde-format
msgid "Contacts"
msgstr "Contactes"

#: src/qml/components/BottomToolbar.qml:105 src/qml/components/Sidebar.qml:74
#: src/qml/DialerPage.qml:27
#, kde-format
msgid "Dialer"
msgstr "Marcador"

#: src/qml/components/Sidebar.qml:98 src/qml/ContactsPage.qml:31
#: src/qml/DialerPage.qml:58 src/qml/HistoryPage.qml:43
#: src/qml/settings/SettingsPage.qml:19
#, kde-format
msgid "Settings"
msgstr "Configuració"

#: src/qml/components/Sidebar.qml:117
#, kde-format
msgid "Quit"
msgstr "Surt"

#: src/qml/ContactsPage.qml:100
#, kde-format
msgid "Select number to call"
msgstr "Seleccioneu el número a trucar"

#: src/qml/ContactsPage.qml:109
#, kde-format
msgid "No contacts have a phone number set"
msgstr "Cap contacte té definit un número de telèfon"

#: src/qml/DialerPage.qml:81
#, kde-format
msgid "Modem devices are not found"
msgstr "No s'ha trobat cap dispositiu de mòdem"

#: src/qml/DialerPage.qml:90
#, kde-format
msgid "Voicemail number couldn't be found"
msgstr "No s'ha pogut trobar el número de la bústia de veu"

#: src/qml/history/HistoryDelegate.qml:61
#, kde-format
msgid "Duration: %1"
msgstr "Durada: %1"

#: src/qml/HistoryPage.qml:18
#, kde-format
msgid "Call History"
msgstr "Historial de trucades"

#: src/qml/HistoryPage.qml:33
#, kde-format
msgid "Clear history"
msgstr "Neteja l'historial"

#: src/qml/HistoryPage.qml:83
#, kde-format
msgid "No recent calls"
msgstr "No hi ha cap trucada recent"

#: src/qml/ImeiSheet.qml:18
#, kde-format
msgid "IMEI"
msgid_plural "IMEIs"
msgstr[0] "IMEI"
msgstr[1] "IMEI"

#: src/qml/ImeiSheet.qml:30
#, kde-format
msgid "No IMEIs found"
msgstr "No s'ha trobat cap IMEI"

#: src/qml/settings/CallBlockSettingsPage.qml:20
#, kde-format
msgid "Adaptive Call Blocking"
msgstr "Bloqueig adaptatiu de trucades"

#: src/qml/settings/CallBlockSettingsPage.qml:54
#, kde-format
msgid "Ignore calls from unknown numbers"
msgstr "Ignora les trucades des de números desconeguts"

#: src/qml/settings/CallBlockSettingsPage.qml:70
#, kde-format
msgid "When a call is an incoming from an unknown number"
msgstr "Quan és una trucada entrant d'un número desconegut"

#: src/qml/settings/CallBlockSettingsPage.qml:74
#, kde-format
msgid "Immediately hang up"
msgstr "Penja immediatament"

#: src/qml/settings/CallBlockSettingsPage.qml:79
#, kde-format
msgid "Ring without notification"
msgstr "Trucada sense notificació"

#: src/qml/settings/CallBlockSettingsPage.qml:84
#, kde-format
msgid "Ring with silent notification"
msgstr "Trucada amb notificació silenciosa"

#: src/qml/settings/CallBlockSettingsPage.qml:100
#, kde-format
msgid "Allowed exceptions"
msgstr "Excepcions permeses"

#: src/qml/settings/CallBlockSettingsPage.qml:106
#, kde-format
msgid "Anonymous numbers"
msgstr "Números anònims"

#: src/qml/settings/CallBlockSettingsPage.qml:113
#, kde-format
msgid "Existing outgoing call to number"
msgstr "Trucada sortint existent al número"

#: src/qml/settings/CallBlockSettingsPage.qml:120
#, kde-format
msgid "Callback within"
msgstr "Trucada de retorn en"

#: src/qml/settings/CallBlockSettingsPage.qml:138
#, kde-format
msgid "minutes"
msgstr "minuts"

#: src/qml/settings/CallBlockSettingsPage.qml:156
#, kde-format
msgid "Allowed phone number exceptions"
msgstr "Excepcions permeses de números de telèfon"

#: src/qml/settings/CallBlockSettingsPage.qml:168
#: src/qml/settings/CallBlockSettingsPage.qml:193
#, kde-format
msgid "Add new pattern"
msgstr "Afegeix un patró nou"

#: src/qml/settings/SettingsPage.qml:57
#, kde-format
msgid "About"
msgstr "Quant a"

#: src/qml/settings/SettingsPage.qml:71
#, kde-format
msgid "Adaptive call blocking"
msgstr "Bloqueig adaptatiu de trucades"

#: src/qml/settings/SettingsPage.qml:80
#, kde-format
msgid "Incoming call screen appearance"
msgstr "Aparença de la pantalla de trucada entrant"

#: src/qml/settings/SettingsPage.qml:81
#, kde-format
msgid "Buttons"
msgstr "Botons"

#: src/qml/settings/SettingsPage.qml:81
#, kde-format
msgid "Symmetric Swipe"
msgstr "Lliscament simètric"

#: src/qml/settings/SettingsPage.qml:81
#, kde-format
msgid "Asymmetric Swipe"
msgstr "Lliscament asimètric"

#: src/qml/USSDSheet.qml:40
#, kde-format
msgid "USSD Error"
msgstr "Error de l'USSD"

#: src/qml/USSDSheet.qml:40
#, kde-format
msgid "USSD Message"
msgstr "Missatge USSD"

#: src/qml/USSDSheet.qml:49
#, kde-format
msgid "Write response..."
msgstr "Escriu una resposta..."

#: src/qml/USSDSheet.qml:54
#, kde-format
msgid "Send"
msgstr "Envia"

#: src/qml/USSDSheet.qml:70
#, kde-format
msgid "Cancel USSD session"
msgstr "Cancel·la la sessió USSD"
